# Spectrum Exchange


## Installation

First, install `go` and `protoc-doc-gen` (https://github.com/pseudomuto/protoc-gen-doc).

To compile the Spectrum Exchange Protocol protobuf run

```
make
```

After this completes, you can install the library for either python or cpp with

```
make cpp_install
```

or 

```
make python_install
```

