#!/usr/bin/env python

from distutils.core import setup

setup(name='sep_pb2',
      author='Zylinium',
      author_email='dev@zylinium.com',
      py_modules = ['sep_pb2']
     )
