# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

all: doc/sep.md doc/sep.html sep_pb2/sep_pb2.py src/sep.pb.cc src/sep.pb.h

cpp_install: src/sep.pb.cc src/sep.pb.h
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 src/sep.pb.cc $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 src/sep.pb.h $(DESTDIR)$(PREFIX)/include/

python_install: sep_pb2/sep_pb2.py
	pip install sep_pb2/

python_uninstall:
	pip uninstall sep_pb2

cpp_uninstall:
	rm $(DESTDIR)$(PREFIX)/include/sep.pb.h
	rm $(DESTDIR)$(PREFIX)/lib/sep.pb.cc

clean:
	rm -f sep_pb2/sep_pb2.py src/sep.pb.cc src/sep.pb.h
	rm -rf doc

sep_pb2/sep_pb2.py src/sep.pb.cc src/sep.pb.h: sep.proto
	protoc --cpp_out=src --python_out=sep_pb2 sep.proto

doc: sep.proto
	mkdir doc

doc/sep.md: sep.proto doc
	protoc --doc_out=markdown,sep.md:doc sep.proto

doc/sep.html: sep.proto doc
	protoc --doc_out=html,sep.html:doc sep.proto
